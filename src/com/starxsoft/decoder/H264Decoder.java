package com.starxsoft.decoder;

public class H264Decoder {

	// public static native int InitCodec(byte bInOneFrameOnce);
	//
	// public static native void UninitCodec();
	//
	// public static native int H264Decode(byte[] out_bmp565, byte[] pRawData,
	// int nRawDataSize, int[] out_4para);

	public static native int InitDecoder(int width, int height);

	public static native int UninitDecoder();

	public static native int DecoderNal(byte[] in, int insize, byte[] out);

	static {
		try {
			System.loadLibrary("H264VideoKit");
		} catch (UnsatisfiedLinkError ule) {
			System.out.println("loadLibrary(H264VideoKit)," + ule.getMessage());
		}
	}
}
